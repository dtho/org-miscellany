# Example of LaTeX_header for org

## suppressing title, adding custom header, and increasing font size w/extsizes

```
#+OPTIONS: toc:nil date:nil author:nil
#+LATEX_COMPILER: xelatex
#+LaTeX_HEADER: \usepackage{extsizes}
#+LaTeX_HEADER: \usepackage{parskip} \usepackage{nopageno} \usepackage{fancyhdr}
#+LATEX_CLASS: extarticle
#+LaTeX_class_options: [14pt]
# +LaTeX_class_options: [12pt]
#+LaTeX_HEADER: \pagestyle{fancy} \fancyhf{} \fancyhead[L]{} \fancyhead[R]{CHEM 311}
#+LATEX_HEADER: \renewcommand\maketitle{}
```

Note that, for extarticle and extsizes, the package must be installed and org must be aware of the class:

(push '("extarticle" "\\documentclass[11pt]{article}"
  ("\\section{%s}" . "\\section*{%s}")
  ("\\subsection{%s}" . "\\subsection*{%s}")
  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
  ("\\paragraph{%s}" . "\\paragraph*{%s}")
  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
org-latex-classes)


## Misc. examples

```
#+LaTeX_HEADER: \usepackage{parskip} \usepackage{nopageno} \usepackage{tabularx}

#+LaTeX_HEADER: \usepackage[margin=1.5cm,portrait,letterpaper]{geometry} \newenvironment{identity} {\endgroup\ignorespaces} {\begingroup\def\@currenvir{identity}\ignorespacesafterend}
```


## Specifying font
```
#+LATEX_COMPILER: xelatex
#+LaTeX_HEADER: \usepackage{xltxtra} \usepackage{fontspec} \setmainfont{DejaVu Serif} \setsansfont{DejaVu Sans} \setmonofont{DejaVu Sans Mono}
```

Access to the libertine and biolinium fonts:

```
#+latex_header: \usepackage{libertine}
```

### Increasing font size:

```
#+latex_header: \usepackage{setspace}
#+latex_header: \onehalfspacing
#+latex_class_options: [12pt]
```

## Adjusting margins

```
#+LaTeX_HEADER: \usepackage[margin=1in,portrait,letterpaper]{geometry}
```



## Adjusting title font size

```
#+LaTeX_HEADER: \usepackage{titling} \pretitle{\begin{flushleft}\LARGE\sffamily}
```

## Page orientation

```
#+LaTeX_HEADER: \usepackage{titling} \pretitle{\begin{flushleft}\LARGE\sffamily}
```



## inputenc as UTF8 encoding should be unnecessary with LaTeX 2018 or later

```
#+latex_header: \usepackage[utf8]{inputenc}
```

## other examples

```
#+TITLE: Week 6: Solubility
#+OPTIONS: toc:nil date:nil author:nil
#+LaTeX_HEADER: \usepackage{parskip} \usepackage{nopageno} \usepackage{fancyhdr}
#+LaTeX_class_options: [12pt,notitlepage]
#+LaTeX_HEADER: \pagestyle{fancy} \fancyhf{} \fancyhead[L]{} \fancyhead[R]{CHEM 212L}
#+LATEX_HEADER: \renewcommand\maketitle{}
```
